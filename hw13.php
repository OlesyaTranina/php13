<?php
//массив животных
$animal = array(
	"Africa" => array("Elephantidae","Hippotigris"),
	"Europe" => array("Canis lupus","Vulpes"),
	"Asia" => array("Petauristinae","Ailuropoda melanoleuca"),
	"Australia" => array("Macropus","Phascolarctos cinereus","Ornithorhynchus anatinus"),
	"America" => array("Bison","Mephitidae","Lama"),
	"Antarctica" => array("Spheniscidae","Leptonychotes weddellii")
	);

//пустой массив куда сложим всех животных с длинными именами
$long_animal = array();
//заполняем массив длинных животных
foreach ($animal as &$value) {
	for($j=0; $j < count($value); $j++) 
	{ 
		if (str_word_count($value[$j]) == 2) {
			$long_animal[] = $value[$j];
		}
	} 
} 
//делим названия на слова
$first_words = array();
$second_words = array();
//var_dump($longanimal);
for ($i=0; $i < count($long_animal);$i++)
{
	$words =	explode(" ", $long_animal[$i]);
	$first_words[]=$words[0];
	$second_words[]=$words[1];
}	
//первые слова
echo "first_words<br/>"; 
var_dump($first_words);
echo "<br/>"; 
//перемешиваем первые слова
shuffle($first_words);
var_dump($first_words);
echo "<br/>"; 
//вторые слова
echo "second_words<br/>"; 
var_dump($second_words);
echo "<br/>"; 
//перемешиваем
shuffle($second_words);
var_dump($second_words);
echo "<br/>"; 
//склеиваем случайное животное
$random_animal = array();
for ($i=0; $i < count($first_words);$i++)
{
  $random_animal[] = $first_words[$i]." ".$second_words[$i];
};
//сравним что получилось
echo "Перемешанные <br/>"; 
var_dump($random_animal);
echo "<br/>"; 
echo "Изначальные  <br/>"; 
var_dump($long_animal);

?>